package Model;

/* $Id$
 *
 * Revisions:
 *   $Log: Lane.java,v $
 *   Revision 1.52  2003/02/20 20:27:45  ???
 *   Fouls disables.
 *
 *   Revision 1.51  2003/02/20 20:01:32  ???
 *   Added things.
 *
 *   Revision 1.50  2003/02/20 19:53:52  ???
 *   Added foul support.  Still need to update laneview and test this.
 *
 *   Revision 1.49  2003/02/20 11:18:22  ???
 *   Works beautifully.
 *
 *   Revision 1.48  2003/02/20 04:10:58  ???
 *   Score reporting code should be good.
 *
 *   Revision 1.47  2003/02/17 00:25:28  ???
 *   Added disbale controls for View objects.
 *
 *   Revision 1.46  2003/02/17 00:20:47  ???
 *   fix for event when game ends
 *
 *   Revision 1.43  2003/02/17 00:09:42  ???
 *   fix for event when game ends
 *
 *   Revision 1.42  2003/02/17 00:03:34  ???
 *   Bug fixed
 *
 *   Revision 1.41  2003/02/16 23:59:49  ???
 *   Reporting of sorts.
 *
 *   Revision 1.40  2003/02/16 23:44:33  ???
 *   added mechnanical problem flag
 *
 *   Revision 1.39  2003/02/16 23:43:08  ???
 *   added mechnanical problem flag
 *
 *   Revision 1.38  2003/02/16 23:41:05  ???
 *   added mechnanical problem flag
 *
 *   Revision 1.37  2003/02/16 23:00:26  ???
 *   added mechnanical problem flag
 *
 *   Revision 1.36  2003/02/16 21:31:04  ???
 *   Score logging.
 *
 *   Revision 1.35  2003/02/09 21:38:00  ???
 *   Added lots of comments
 *
 *   Revision 1.34  2003/02/06 00:27:46  ???
 *   Fixed a race condition
 *
 *   Revision 1.33  2003/02/05 11:16:34  ???
 *   Boom-Shacka-Lacka!!!
 *
 *   Revision 1.32  2003/02/05 01:15:19  ???
 *   Real close now.  Honest.
 *
 *   Revision 1.31  2003/02/04 22:02:04  ???
 *   Still not quite working...
 *
 *   Revision 1.30  2003/02/04 13:33:04  ???
 *   Lane may very well work now.
 *
 *   Revision 1.29  2003/02/02 23:57:27  ???
 *   fix on pinsetter hack
 *
 *   Revision 1.28  2003/02/02 23:49:48  ???
 *   Pinsetter generates an event when all pins are reset
 *
 *   Revision 1.27  2003/02/02 23:26:32  ???
 *   ControlDesk now runs its own thread and polls for free lanes to assign queue members to
 *
 *   Revision 1.26  2003/02/02 23:11:42  ???
 *   parties can now play more than 1 game on a lane, and lanes are properly released after games
 *
 *   Revision 1.25  2003/02/02 22:52:19  ???
 *   Lane compiles
 *
 *   Revision 1.24  2003/02/02 22:50:10  ???
 *   Lane compiles
 *
 *   Revision 1.23  2003/02/02 22:47:34  ???
 *   More observering.
 *
 *   Revision 1.22  2003/02/02 22:15:40  ???
 *   Add accessor for pinsetter.
 *
 *   Revision 1.21  2003/02/02 21:59:20  ???
 *   added conditions for the party choosing to play another game
 *
 *   Revision 1.20  2003/02/02 21:51:54  ???
 *   LaneEvent may very well be observer method.
 *
 *   Revision 1.19  2003/02/02 20:28:59  ???
 *   fixed sleep thread bug in lane
 *
 *   Revision 1.18  2003/02/02 18:18:51  ???
 *   more changes. just need to fix scoring.
 *
 *   Revision 1.17  2003/02/02 17:47:02  ???
 *   Things are pretty close to working now...
 *
 *   Revision 1.16  2003/01/30 22:09:32  ???
 *   Worked on scoring.
 *
 *   Revision 1.15  2003/01/30 21:45:08  ???
 *   Fixed speling of received in Lane.
 *
 *   Revision 1.14  2003/01/30 21:29:30  ???
 *   Fixed some MVC stuff
 *
 *   Revision 1.13  2003/01/30 03:45:26  ???
 *   *** empty log message ***
 *
 *   Revision 1.12  2003/01/26 23:16:10  ???
 *   Improved thread handeling in lane/controldesk
 *
 *   Revision 1.11  2003/01/26 22:34:44  ???
 *   Total rewrite of lane and pinsetter for R2's observer model
 *   Added Lane/Pinsetter Observer
 *   Rewrite of scoring algorythm in lane
 *
 *   Revision 1.10  2003/01/26 20:44:05  ???
 *   small changes
 *
 * 
 */

import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.HashMap;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import ViewControl.additionFrame;
import ViewControl.emoticon;

public class Lane extends Observable implements Runnable, Observer,Serializable {
	public Party party;
	private Pinsetter setter;
	private HashMap scores;
	private boolean gameIsHalted;

	public boolean partyAssigned;
	private boolean gameFinished;
	private Iterator bowlerIterator;
	private int ball;
	private int bowlIndex;
	private int frameNumber;
	private boolean tenthFrameStrike;

	// private int[] curScores;
	private int[][] cumulScores;
	private boolean canThrowAgain;
	private boolean firstGutter = false;

	private int max_till_now = 0;
	// public int[][] finalScores;
	public int gameNumber;

	private Bowler currentThrower; // = the thrower who just took a throw
	private ScoreCalculator sc;
	private int maxScore_idx;
	private int maxScore_idx_second;
	private int penalty = 0;
	private boolean simulate=false;
	private boolean throwBall=true;

	/**
	 * Lane()
	 * 
	 * Constructs a new lane and starts its thread
	 * 
	 * @pre none
	 * @post a new lane has been created and its thered is executing
	 */
	public Lane() {
		setter = new Pinsetter();
		scores = new HashMap();

		gameIsHalted = false;
		partyAssigned = false;
		gameNumber = 0;
		setter.addObserver(this);
		Thread laneThread = new Thread(this);
		laneThread.start();
	}

	/*
	 * Simulate Ball Throw
	 */
	public int getpinScore() {
		int count = 0;
		boolean pins[] = new boolean[10];
		Random rnd = new Random();
		boolean foul = false;
		for (int i = 0; i <= 9; i++) {
			pins[i] = true;
		}
		double skill = rnd.nextDouble();
		for (int i = 0; i <=4; i++) {
			if (pins[i]) {
				double pinluck = rnd.nextDouble();
				if (pinluck <= .04) {
					foul = true;
				}
				if (((skill + pinluck) / 2.0 * 1.2) > .5) {
					pins[i] = false;
				}
				if (!pins[i]) { // this pin just knocked down
					count++;
				}
			}
		}
		return count;
	}
	
	public void winnerPrompt(String msg) {
        JFrame jFrame = new JFrame("Winner of Game");
        JOptionPane.showMessageDialog(jFrame, msg);
	}

	/**
	 * run()
	 * 
	 * entry point for execution of this lane
	 */
	public void run() {

		while (true) {
			if (partyAssigned) {
				if (!gameFinished) { // we have a party on this lane,
					// so next bower can take a throw
					while (gameIsHalted) {
						try {
							// sleep(10);
							Thread.sleep(10);
						} catch (Exception e) {
						}
					}

					if (bowlerIterator.hasNext()) {
						currentThrower = (Bowler) bowlerIterator.next();
						canThrowAgain = true;
						tenthFrameStrike = false;
						ball = 0;
//						while (canThrowAgain) {
//								setter.ballThrown(); // simulate the thrower's ball hiting
//								ball++;
//							
//						}
						
						while (canThrowAgain ) {
							try {
							Thread.sleep(10);
						} catch (Exception e) {}
							if(throwBall || simulate) {
								setter.ballThrown(); // simulate the thrower's ball hiting
								ball++;
							}
							throwBall=false;
							
						}
						

						// Penalty calculation starts

						int sc[] = (int[]) scores.get(currentThrower);
						int prev_chance = sc[2 * frameNumber];
						int curr_chance = sc[2 * frameNumber + 1];
						if (frameNumber == 0) {
							if (prev_chance == 0 && prev_chance == curr_chance) {
								firstGutter = true;
								System.out.println("First Two Score is zero");
							}
							max_till_now = Math.max(max_till_now, cumulScores[bowlIndex][frameNumber]);
						} 
						else if (frameNumber >= 1) {
							if (firstGutter) {
								System.out.println("FrameNumber: "+ frameNumber);
								System.out.println("CumuLScore Before: "+ cumulScores[bowlIndex][frameNumber]);
								penalty = cumulScores[bowlIndex][frameNumber] / 2;
								cumulScores[bowlIndex][frameNumber] -= penalty;
								firstGutter = false;
								
							} else if (prev_chance == 0 && ((prev_chance == sc[(2 * frameNumber) - 1])
									|| (prev_chance == curr_chance))) {
								System.out.println("FrameNumber: "+ frameNumber);
								System.out.println("CumuLScore Before: "+ cumulScores[bowlIndex][frameNumber]);
								System.out.println("CumuLScore After: "+ cumulScores[bowlIndex][frameNumber] );
							}
							max_till_now = Math.max(max_till_now, cumulScores[bowlIndex][frameNumber]- cumulScores[bowlIndex][frameNumber-1]);
							if(penalty!=0) {
								if(penalty % 2 == 0) {
									sc[2*frameNumber] -= penalty / 2;
									sc[2*frameNumber + 1] -= penalty/ 2;
								}
								else{
									sc[2*frameNumber] -= (penalty / 2) + 1;
									sc[2*frameNumber + 1] -= penalty/ 2;
								}
								scores.put(currentThrower,sc);
								penalty = 0;
							}
						}

						// penalty calculation ends

						if (frameNumber == 9) {
							try {
								Date date = new Date();
								String dateString = "" + date.getHours() + ":" + date.getMinutes() + " "
										+ date.getMonth() + "/" + date.getDay() + "/" + (date.getYear() + 1900);
								ScoreHistoryFile.addScore(currentThrower.getNick(), dateString,
										new Integer(cumulScores[bowlIndex][9]).toString());
							} catch (Exception e) {
								System.err.println("Exception in addScore. " + e);
							}
						}

						setter.reset();

						bowlIndex++;

					} else {
						frameNumber++;
						bowlIndex = 0;
						if (frameNumber == 10) {
							int curr_idx = 0;
							Iterator scoreIt1 = getParty().getMembers().iterator();
							int n_temp = getParty().getMembers().size();
							if (n_temp > 1) {
								Bowler maxScore_bowler = (Bowler) scoreIt1.next();
								Bowler second_maxScore_bowler = null;
								maxScore_idx = curr_idx;
								maxScore_idx_second = -1;
								int maxm = getCumulScore()[curr_idx++][9], secondmaxm = -1;
								while (scoreIt1.hasNext()) {
									Bowler thisBowler = (Bowler) scoreIt1.next();
									int temp = getCumulScore()[curr_idx][9];
									if (maxm < temp) {
										second_maxScore_bowler = maxScore_bowler;
										maxScore_bowler = thisBowler;
										maxScore_idx_second = maxScore_idx;
										maxScore_idx = curr_idx;
										secondmaxm = maxm;
										maxm = temp;
									} else if (secondmaxm < temp) {
										secondmaxm = temp;
										second_maxScore_bowler = thisBowler;
										maxScore_idx_second = curr_idx;
									}
									curr_idx += 1;
								}
								System.out.println("After 10 frames");
								System.out.println("Curr Max: " + maxScore_bowler.getNick() + " Score: " + maxm);
								System.out.println(
										"Curr Max: " + second_maxScore_bowler.getNick() + " Score: " + (secondmaxm));
								String msg = "Winner of Game After 10 Chances: " + maxScore_bowler.getNick();
								winnerPrompt(msg);
								Vector<String> player = new Vector();
								player.add(maxScore_bowler.getNick());
								player.add(second_maxScore_bowler.getNick());
								additionFrame additional = new additionFrame();
								additional.receiveLaneEvent(player);
								additional.show();
								int[] score_val = new int[2];

								canThrowAgain = true;
								tenthFrameStrike = false;
								ball = -1;
								int bowlerID = 1;
								int frame_no = 0;
//								boolean[] prev_strike = new boolean[] { false, false };
								boolean value = true;

								int[] cell_no = new int[] { 2, 0 };
								while (value && frame_no < 4) {
									int score = getpinScore();
									if (score == 10) {
										 emoticon emotion = new emoticon();
										 emotion.wow();
										 emotion.show();
										 emotion = null;
									} else if (score == 0 || score==1) {
										 emoticon emotion = new emoticon();
										 emotion.sad();
										 emotion.show();
										 emotion = null;
									}
									else if(score>=7 && score<=9) {
										 emoticon emotion = new emoticon();
										 emotion.sad();
										 emotion.show();
										 emotion = null;
									}
									System.out.format("Score:%d\n", score);
									if (ball == -1) {
										additional.changeBall(bowlerID, score, cell_no[1]++);
										int temp = getpinScore();
										System.out.format("Score:%d\n", temp);
										additional.changeBall(bowlerID, temp, cell_no[1]++);
										score += temp;
										additional.changeScore(bowlerID, secondmaxm + score, frame_no);
										frame_no+=1;
									} else {
										additional.changeBall(bowlerID, score, cell_no[bowlerID]++);
									}
									score_val[bowlerID] += score;
									if (secondmaxm + score > maxm && ball == -1) {
										score_val[bowlerID] = secondmaxm;
										score_val[bowlerID] += score;
										if (bowlerID == 1)
											bowlerID = 0;
										score_val[bowlerID] = maxm;
										ball = 0;

									} else if (ball == -1)
										value = false;
									else {
										ball++;
										if (ball == 2) {
											additional.changeScore(bowlerID, score_val[bowlerID], frame_no);
											ball = 0;
											if (bowlerID == 1)
												frame_no++;
											if (bowlerID == 0)
												bowlerID = 1;
											else
												bowlerID = 0;
										}
									}

									try {
										Thread.sleep(100);
									} catch (Exception e) {
									}
								}
								if (ball != -1) {
									cumulScores[maxScore_idx][9] = score_val[0];
									cumulScores[maxScore_idx_second][9] = score_val[1];
								}
								if(cumulScores[maxScore_idx][9] > cumulScores[maxScore_idx_second][9]) {
									msg = "Winner of Game After Additonal Throw: " + maxScore_bowler.getNick();
									winnerPrompt(msg);
								}
								else {
									msg = "Winner of Game After Additonal Throw: " + second_maxScore_bowler.getNick();
									winnerPrompt(msg);
								}
							}
							frameNumber++;
						}
						bowlIndex = 0;
						resetBowlerIterator();
						if (frameNumber > 10) {
							gameFinished = true;
							gameNumber++;
							simulate = false;
							throwBall = true;
						}
					}

				}

				else if (gameFinished) {
					publish();
//					EndGamePrompt egp = new EndGamePrompt( ((Bowler) party.getMembers().get(0)).getNick() + "'s Party" );
//					int result = egp.getResult();
//					egp.distroy();
//					egp = null;
//					
//					/*
//					
//					 */
//
//					
//					System.out.println("result was: " + result);
//					
//					// TODO: send record of scores to control desk
//					if (result == 1) {					// yes, want to play again
//						resetScores();
//						resetBowlerIterator();
//						
//					} else if (result == 2) {// no, dont want to play another game
//						Vector printVector;	
//						EndGameReport egr = new EndGameReport( ((Bowler)party.getMembers().get(0)).getNick() + "'s Party", party);
//						printVector = egr.getResult();
//						partyAssigned = false;
//						Iterator scoreIt = party.getMembers().iterator();
//						party = null;
//						partyAssigned = false;
//						
////						publish();
//						
//						int myIndex = 0;
//						while (scoreIt.hasNext()){
//							Bowler thisBowler = (Bowler)scoreIt.next();
//							ScoreReport sr = new ScoreReport( thisBowler, getCumulScore()[myIndex++], gameNumber );
//							sr.sendEmail(thisBowler.getEmail());
//							Iterator printIt = printVector.iterator();
//							while (printIt.hasNext()){
//								if (thisBowler.getNick() == (String)printIt.next()){
//									System.out.println("Printing " + thisBowler.getNick());
//									sr.sendPrintout();
//								}
//							}
//	
//						}
//					}
				}
				//
			}
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}
		}
	}

	/**
	 * recievePinsetterEvent()
	 * 
	 * recieves the thrown event from the pinsetter
	 *
	 * @pre none
	 * @post the event has been acted upon if desiered
	 * 
	 * @param pe The pinsetter event that has been received.
	 */

	/**
	 * resetBowlerIterator()
	 * 
	 * sets the current bower iterator back to the first bowler
	 * 
	 * @pre the party as been assigned
	 * @post the iterator points to the first bowler in the party
	 */
	public void resetBowlerIterator() {
		bowlerIterator = (party.getMembers()).iterator();
	}

	/**
	 * resetScores()
	 * 
	 * resets the scoring mechanism, must be called before scoring starts
	 * 
	 * @pre the party has been assigned
	 * @post scoring system is initialized
	 */
	//

	public void resetScores() {
		Iterator bowlIt = (party.getMembers()).iterator();

		while (bowlIt.hasNext()) {
			int[] toPut = new int[25];
			for (int i = 0; i != 25; i++) {
				toPut[i] = -1;
			}
			scores.put(bowlIt.next(), toPut);
		}

		gameFinished = false;
		frameNumber = 0;
	}

	/**
	 * assignParty()
	 * 
	 * assigns a party to this lane
	 * 
	 * @pre none
	 * @post the party has been assigned to the lane
	 * 
	 * @param theParty Party to be assigned
	 */
	public void assignParty(Party theParty) {
		party = theParty;
		resetBowlerIterator();
		partyAssigned = true;

		// curScores = new int[party.getMembers().size()];
		cumulScores = new int[party.getMembers().size()][10];
		// finalScores = new int[party.getMembers().size()][128]; //Hardcoding a max of
		// 128 games, bite me.
		this.sc = new ScoreCalculator(cumulScores);
		gameNumber = 0;

		resetScores();
	}

	/**
	 * markScore()
	 *
	 * Method that marks a bowlers score on the board.
	 * 
	 * @param Cur   The current bowler
	 * @param frame The frame that bowler is on
	 * @param ball  The ball the bowler is on
	 * @param score The bowler's score
	 */
	private void markScore(Bowler Cur, int frame, int ball, int score) {
		int[] curScore;
		int index = ((frame - 1) * 2 + ball);
		
		
		curScore = (int[]) scores.get(Cur);
		curScore[index - 1] = score;
		scores.put(Cur, curScore);
		
		// getScore( Cur, frame );
		sc.calculateGame((int[]) this.scores.get(Cur), this.bowlIndex, this.frameNumber, ball, score);
		publish();
	}

	/**
	 * lanePublish()
	 *
	 * Method that creates and returns a newly created laneEvent
	 * 
	 * @return The new lane event
	 */

	/**
	 * getScore()
	 *
	 * Method that calculates a bowlers score
	 * 
	 * @param Cur   The bowler that is currently up
	 * @param frame The frame the current bowler is on
	 * 
	 * @return The bowlers total score
	 */

	public boolean isMechanicalProblem() {
		return gameIsHalted;
	}

	public int getFrameNum() {
		return frameNumber + 1;
	}

	public HashMap getScore() {
		return scores;
	}

	public int getIndex() {
		return bowlIndex;
	}

	public int getBall() {
		return ball;
	}

	public int[][] getCumulScore() {
		return cumulScores;
	}

	public Party getParty() {
		return party;
	}

	public Bowler getBowler() {
		return currentThrower;
	}

	/**
	 * isPartyAssigned()
	 * 
	 * checks if a party is assigned to this lane
	 * 
	 * @return true if party assigned, false otherwise
	 */
	public boolean isPartyAssigned() {
		return partyAssigned;
	}

	/**
	 * isGameFinished
	 * 
	 * @return true if the game is done, false otherwise
	 */
	public boolean isGameFinished() {
		return gameFinished;
	}

	/**
	 * publish
	 *
	 * Method that publishes an event to subscribers
	 * 
	 * @param event Event that is to be published
	 */

	public void publish() {
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Accessor to get this Lane's pinsetter
	 * 
	 * @return A reference to this lane's pinsetter
	 */

	public Pinsetter getPinsetter() {
		return setter;
	}

	/**
	 * Pause the execution of this game
	 */
	public void pauseGame() {
		gameIsHalted = true;
		publish();
	}

	/**
	 * Resume the execution of this game
	 */
	public void unPauseGame() {
		gameIsHalted = false;
		publish();
	}
	public void simulateGame() {
		this.simulate = true;
	}
	
	public void throwBall() {
		this.throwBall = true;
	}
	
public void saveAndExit(String filename) {
		
		if (gameIsHalted == false) {
			return;
		}

//		try {
//			FileOutputStream file = new FileOutputStream (filename); 
//			ObjectOutputStream out = new ObjectOutputStream (file); 
//
//			// Method for serialization of object 
//			out.writeObject(this); 
//
//			out.close(); 
//			file.close();
			System.out.println("Game Saved");
//		} 

//		catch (IOException ex) { 
//			System.out.println("IOException is caught" + ex); 
//		} 
//		
//		catch (Exception e) {
//			System.out.println("Exception is caught" + e); 	
//		}
		
		resetScores();
		resetBowlerIterator();
		partyAssigned = false;
		party = null;
		partyAssigned = false;
	}


	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		Pinsetter pe = (Pinsetter) arg;
		if (pe.pinsDownOnThisThrow() >= 0) { // this is a real throw
			markScore(currentThrower, frameNumber + 1, pe.getThrowNumber(), pe.pinsDownOnThisThrow());

			// next logic handles the ?: what conditions dont allow them another throw?
			// handle the case of 10th frame first
			if (frameNumber == 9) {
				if (pe.totalPinsDown() == 10) {
					setter.resetPins();
					if (pe.getThrowNumber() == 1) {
						tenthFrameStrike = true;
					}
				}

				if (((pe.totalPinsDown() != 10) && (pe.getThrowNumber() == 2 && tenthFrameStrike == false))
						|| pe.getThrowNumber() == 3) {
					canThrowAgain = false;
					// publish( lanePublish() );
				}

			} else { // its not the 10th frame

				if (pe.pinsDownOnThisThrow() == 10 || pe.getThrowNumber() == 2) { // threw a strike
					canThrowAgain = false;
					// publish( lanePublish() );
				} else if (pe.getThrowNumber() == 3)
					System.out.println("I'm here...");
			}
		}

	}

}
