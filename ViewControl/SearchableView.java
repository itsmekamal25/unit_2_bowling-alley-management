package ViewControl;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import java.io.*;


public class SearchableView{
	
	private Label x;
	private TextField t;
	static Choice c;
	
	SearchableView() {
		String s="Searchable View";
		Frame f = new Frame(s);
		s="Select query:";
		Label l = new Label(s);
		c = new Choice();
		c.setBounds(100, 100, 75, 75); 
		
		s="Enter Nick Name";
		x=new Label(s);
		s="                  ";
		t = new TextField(s);
		t.setBounds(150, 100, 100, 30);
		s="Search";
		Button b = new Button(s);
		b.setBounds(100, 200, 100, 50);
		
//		s="Note: Use NickName TextField for Specific Player";
//		Label note=new Label(s);
//		note.setFont(new Font("Verdana", Font.PLAIN, 9));
//		note.setBounds(150,100,100,30);
		s="Result: ";
		JLabel r = new JLabel(s);  // Result.
	    r.setVerticalAlignment(JLabel.TOP);
//	      label.setFont(new Font("Verdana", Font.PLAIN, 15));
	    r.setBackground(new Color(255, 255, 255));
	    r.setPreferredSize(new Dimension(250, 100));
		r.setBounds(100,300,70,70);
	     Border border = BorderFactory.createLineBorder(Color.ORANGE);
	    r.setBorder(border);
		
		s="Highest score all";
		c.add(s);
		s="Lowest score all";
		c.add(s);
		s="Top player";
		c.add(s);
		s="Highest score specific";
		c.add(s);
		s="Lowest score specific";
		c.add(s);
		s="Last 5 score specific";
		c.add(s);
		

		
		b.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ae) {
				String query = c.getItem(c.getSelectedIndex());
				String result;

				if (query.equals("Highest score all")) {
					try {
						result = "Result: " + SearchDatabase.giveHighestScoreAll();
					} 
					catch (Exception e) {
						String s="Error retriving highest score.";
						result = s;
					}
				}
				else if (query.equals("Lowest score all")) {
					try {
						result = "Result: " + SearchDatabase.giveLowestScoreAll();
					} 
					catch (Exception e) {
						String s="Error retriving lowest score.";
						result = s;
					}
				}
				else if (query.equals("Top player")) {
					try {
						result = "Result: " + SearchDatabase.giveTopPlayer();
					} 
					catch (Exception e) {
						String s="Error retriving top player.";
						result = s;
						
					}
				}
				else if (query.equals("Highest score specific")) {
//					if(t.isEditable() == false)
//						t.setEditable(true);
					try {
						String playerName = t.getText();
						playerName = playerName.trim();
						playerName = playerName.toLowerCase();
						result = "Result: " + SearchDatabase.giveHighestScoreSpecific(playerName);
					} 
					catch (Exception e) {
						String s="Error retriving top player.";
						result = s;
					}
				}
				else if (query.equals("Lowest score specific")) {
					try {
						String playerName = t.getText();
						playerName = playerName.trim();
						playerName = playerName.toLowerCase();
						result = "Result: " + SearchDatabase.giveLowestScoreSpecific(playerName);
					} 
					catch (Exception e) {
						String s="Error retriving top player.";
						result = s;
					}
				}
				else if (query.equals("Last 5 score specific")) {
					try {
						String playerName = t.getText();
						playerName = playerName.trim();
						playerName = playerName.toLowerCase();
						result = "Result: " + SearchDatabase.giveLast5ScoreSpecific(playerName);
					} 
					catch (Exception e) {
						String s="Error retriving top player.";
						result = s;
					}
				}
				else {
					result = "Error in SearchableView.";
				}

//				r.setText(result);
				r.setText("<html>" + result.replaceAll("<","&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br/>") + "</html>");

			}
		});
		

		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				f.dispose();
			}
		});
		JLabel br = new JLabel();
		f.add(l);
		f.add(c);
		f.add(x);
		f.add(t);
//		f.add(nickPanel);
		f.add(br);
		f.add(b);
//		f.add(note);
		f.add(br);
		f.add(r);
	
		f.setLayout(new FlowLayout());
		f.setSize(600, 300);

		f.setVisible(true);
	}
	

}



class SearchDatabase {

	private static String filePath = "C:\\Users\\Acer\\eclipse-workspace\\Bolling-Alley-Management\\SCOREHISTORY.DAT";
	public static String giveHighestScoreSpecific(String playerName) throws FileNotFoundException, IOException {
		File myObj = new File(filePath);
		Scanner myReader = new Scanner(myObj);
		
		int highScore = -1;
		String returnString = "No high score!";
		
		while (myReader.hasNextLine()) {
			String data = myReader.nextLine();
			String[] splitData = data.split("\t");
			int curScore = Integer.parseInt(splitData[2]);
			String curPlayer = splitData[0];
			curPlayer = curPlayer.trim();
			curPlayer = curPlayer.toLowerCase();
			if (curPlayer.equals(playerName) && highScore < curScore) {
				highScore = curScore;
				returnString = splitData[2] +  " on "  + splitData[1];
			}
		}

		return returnString;
	}	


	public static String giveLowestScoreSpecific(String playerName) throws FileNotFoundException, IOException {
//		File myObj = new File("SCOREHISTORY.DAT");
		File myObj = new File(filePath);
		Scanner myReader = new Scanner(myObj);
		
		int lowScore = 301;
		String returnString = "No low score!";
		
		while (myReader.hasNextLine()) {
			String data = myReader.nextLine();
			String[] splitData = data.split("\t");
			int curScore = Integer.parseInt(splitData[2]);
			String curPlayer = splitData[0];
			curPlayer = curPlayer.trim();
			curPlayer = curPlayer.toLowerCase();

			if (curPlayer.equals(playerName) && lowScore > curScore) {
				lowScore = curScore;
				returnString = splitData[2] +  " on "  + splitData[1];
			}
		}

		return returnString;
	}


	public static String giveLast5ScoreSpecific(String playerName) throws FileNotFoundException, IOException {
//		File myObj = new File("SCOREHISTORY.DAT");
		File myObj = new File(filePath);
		Scanner myReader = new Scanner(myObj);
		
		String returnString = "\n";

		java.util.List<String> reverseFile = new ArrayList<String>();

		while (myReader.hasNextLine()) {
			String data = myReader.nextLine();
			reverseFile.add(data);
		}

		int count = 0;
		for(int i = reverseFile.size()-1; i >= 0; i--) {
			String data = reverseFile.get(i);
			String[] splitData = data.split("\t");
			String curPlayer = splitData[0];
			curPlayer = curPlayer.trim();
			curPlayer = curPlayer.toLowerCase();
			if (curPlayer.equals(playerName)) {
				returnString += splitData[2] + " on " + splitData[1] + "\n";
				count += 1;
			}
			
//			returnString+="</html>";

			if (count == 5) {
				break;
			}
		}
	
		if (count == 0) {
			returnString = "\nNo scores found!";
		}

		return returnString;
	}	


	public static String giveHighestScoreAll() throws FileNotFoundException, IOException {
//		File myObj = new File("SCOREHISTORY.DAT");
		File myObj = new File(filePath);
		Scanner myReader = new Scanner(myObj);
		
		int highScore = -1;
		String returnString = "\nNo high score!";
		
		while (myReader.hasNextLine()) {
			String data = myReader.nextLine();
			String[] splitData = data.split("\t");
			int curScore = Integer.parseInt(splitData[2]);
			
			if (highScore < curScore) {
				highScore = curScore;
				returnString = "\n" + splitData[2] + " by " + splitData[0] + " on " +  splitData[1];
			}
		}

		return returnString;
	}


	public static String giveLowestScoreAll() throws FileNotFoundException, IOException {
//		File myObj = new File("SCOREHISTORY.DAT");
		File myObj = new File(filePath);
		Scanner myReader = new Scanner(myObj);
		
		int lowScore = 301;
		String returnString = "\nNo low score!";
		
		while (myReader.hasNextLine()) {
			String data = myReader.nextLine();
			String[] splitData = data.split("\t");
			int curScore = Integer.parseInt(splitData[2]);
			
			if (lowScore > curScore) {
				lowScore = curScore;
				returnString = "\n" + splitData[2] + " by " + splitData[0] + " on " +  splitData[1];
			}
		}

		return returnString;
	}


	public static String giveTopPlayer() throws FileNotFoundException, IOException {
//		File myObj = new File("SCOREHISTORY.DAT");
		File myObj = new File(filePath);
		Scanner myReader = new Scanner(myObj);
		
		int highScore = -1;
		String returnString = "\nNo top player!";
		
		while (myReader.hasNextLine()) {
			String data = myReader.nextLine();
			String[] splitData = data.split("\t");
			int curScore = Integer.parseInt(splitData[2]);
			
			if (highScore < curScore) {
				highScore = curScore;
				returnString = "\n" + splitData[0] + " with score " + splitData[2];
			}
		}

		return returnString;
	}

}