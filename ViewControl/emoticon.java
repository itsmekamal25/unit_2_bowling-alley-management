package ViewControl;
import java.awt.*;
import javax.swing.*;
import java.lang.Thread;
public class emoticon {
	private JFrame frame;
	private ImageIcon img;
	
	public emoticon() {
		frame = new JFrame("Emoticon");
		frame.setSize(310,310);
		frame.getContentPane().setLayout(new BorderLayout());
		((JPanel) frame.getContentPane()).setOpaque(false);
		Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
		
        frame.setLocation(
                ((screenSize.width) / 2) - ((frame.getSize().width) / 2),
                0);
		//frame.setLocationRelativeTo(null);		
	}
	
	public void show(){
		frame.show();
		try {
			Thread.sleep(600);
		} catch (Exception e) {}
		hide();
	}
	
	public void hide() {
		frame.hide();
	}
	
	public void sad() {
		img = new ImageIcon(this.getClass().getResource("sad.png"));
		JLabel label = new JLabel(img, JLabel.CENTER);
		frame.add(label);
	}
	
	public void good() {
		img = new ImageIcon(this.getClass().getResource("good.png"));
		JLabel label = new JLabel(img, JLabel.CENTER);
		frame.add(label);
	}
	
	public void wow() {
		img = new ImageIcon(this.getClass().getResource("wow.png"));
		JLabel label = new JLabel(img, JLabel.CENTER);
		frame.add(label);
	}
}

