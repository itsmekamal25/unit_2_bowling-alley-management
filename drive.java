import java.io.*;
import Model.*;
import ViewControl.*;
//public class drive {
//
//	public static void main(String[] args) {
//
//		int numLanes = 3;
//		int maxPatronsPerParty=5;
//
//		ControlDesk controlDesk = new ControlDesk( numLanes );
//
//		ControlDeskView cdv = new ControlDeskView( controlDesk, maxPatronsPerParty);
//		controlDesk.addObserver( cdv );
//	}
//}

import java.awt.*;
import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class drive {
////
////	
////	transient ActionListener actionListener;
	TextField tf1,tf2;  
    Button b1;
    Label l1,l2; 
    Frame f;
//    
    public drive()
    {
    	f = new Frame("Taking input");
    	l1=new Label("lanes size ");  
    	l1.setBounds(50,40,100,30);   
        l2=new Label("maxpatron size");  
        l2.setBounds(50,100, 120,30);  
        
    	tf1=new TextField();  
        tf1.setBounds(50,70,150,20);
        tf2=new TextField();  
        tf2.setBounds(50,130,150,20); 
        b1=new Button("enter");  
        b1.setBounds(50,200,50,50);  
        b1.addActionListener(new ActionListener() {
       
		@Override
		public void actionPerformed(ActionEvent e) {
	    		String s1=tf1.getText();  
		        String s2=tf2.getText(); 
 
		        int numLanes =   Integer.parseInt(s1); 
				
		        int maxPatronsPerParty = Integer.parseInt(s2); 
				ControlDesk controlDesk = new ControlDesk( numLanes );
				ControlDeskView cdv = new ControlDeskView( controlDesk, maxPatronsPerParty);
				controlDesk.addObserver( cdv );
				hide();
	    	}
        });
        
        f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				f.dispose();
			}
		});
        
        f.add(tf1);
        f.add(tf2);
        f.add(b1);
        f.add(l1);
        f.add(l2);
        f.setSize(300,300);
        f.setLocationRelativeTo(null);
        f.setLayout(null);  
        f.setVisible(true); 
    }
    
    public void hide() {
    	f.hide();
    }

//
public static void main(String[] args){
	
	drive d = new drive();
	
}

}